﻿using System;

namespace Calculadora
{
    internal class Base2 : Base
    {

        private string Valor { get; set; }

        public Base2()
        {
            NumeroBase = 2;
            Valor = "0";
        }

        public Base2(string valor)
        {
            NumeroBase = 2;
            Valor = valor;
        }

        public static Base2 operator +(Base2 a, Base2 b)
        {
         
                var aBase10 = a.ConvertirEnBase10(a.Valor, a.NumeroBase);
                var bBase10 = b.ConvertirEnBase10(b.Valor, a.NumeroBase);
                var resultBase10 = Convert.ToInt32(aBase10) + Convert.ToInt32(bBase10);
                var resultBase2 = a.ConvertirDeBase10ABaseEspecifica(resultBase10, a.NumeroBase);
                return new Base2(resultBase2);
        }

        public static Base2 operator -(Base2 a, Base2 b)
        {

                var aBase10 = a.ConvertirEnBase10(a.Valor, a.NumeroBase);
                var bBase10 = b.ConvertirEnBase10(b.Valor, b.NumeroBase);
                var resultBase10 = Convert.ToInt32(aBase10) - Convert.ToInt32(bBase10);
                var resultBase2 = a.ConvertirDeBase10ABaseEspecifica(resultBase10, a.NumeroBase);
                return new Base2(resultBase2);
        }

        public static Base2 operator /(Base2 a, Base2 b)
        {
            var aBase10 = a.ConvertirEnBase10(a.Valor, a.NumeroBase);
            var bBase10 = b.ConvertirEnBase10(b.Valor, b.NumeroBase);
            var resultBase10 = Convert.ToInt32(aBase10) / Convert.ToInt32(bBase10);
            var resultBase2 = a.ConvertirDeBase10ABaseEspecifica(resultBase10, a.NumeroBase);
            return new Base2(resultBase2);
        }

        public static Base2 operator *(Base2 a, Base2 b)
        {
            var aBase10 = a.ConvertirEnBase10(a.Valor, a.NumeroBase);
            var bBase10 = b.ConvertirEnBase10(b.Valor, a.NumeroBase);
            var resultBase10 = Convert.ToInt32(aBase10) * Convert.ToInt32(bBase10);
            var resultBase2 = a.ConvertirDeBase10ABaseEspecifica(resultBase10, a.NumeroBase);
            return new Base2(resultBase2);
        }


    }



}

