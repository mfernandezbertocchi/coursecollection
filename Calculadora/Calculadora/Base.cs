﻿using System;

namespace Calculadora
{
    public abstract class Base
    {
        private int NumeroBase { get; set; }

        public string ConvertirEnBase10(string valor, int baseDelValor)
        {
            var result = Convert.ToInt32(valor, baseDelValor);
            return result.ToString();
        }

        public string ConvertirDeBase10ABaseEspecifica(int valorEnBase10, int baseEspecifica)
        {
            var result = Convert.ToString(valorEnBase10, baseEspecifica);
            return result;
        }

    }
}
