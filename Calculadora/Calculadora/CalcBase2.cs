﻿
using System;

namespace Calculadora
{
    public class CalcBase2 : ICalculadora
    {

        public string Suma(string a, string b)
        {
            var resultado = new Base2();
            try
            {
                var numero1 = new Base2(a);
                var numero2 = new Base2(b);
                resultado = numero1 + numero2;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }

            return resultado.Valor;
        }
        public string Resta(string a, string b)
        {
            var resultado = new Base2();
            try
            {
                var numero1 = new Base2(a);
                var numero2 = new Base2(b);
                resultado = numero1 - numero2;
            }
            catch (Exception e)
            {
               throw new Exception(e.Message);

            }
            return resultado.Valor;
        }
        public string Dividir(string a, string b)
        {
            var resultado = new Base2();
            try
            {
                var numero1 = new Base2(a);
                var numero2 = new Base2(b);
                resultado = numero1 / numero2;
            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return resultado.Valor;
        }
        public string Multiplicar(string a, string b)
        {
            var resultado = new Base2();
            try
            {
                var numero1 = new Base2(a);
                var numero2 = new Base2(b);
                resultado = numero1 * numero2;

            }
            catch (Exception e)
            {
                throw new Exception(e.Message);
            }
            return resultado.Valor;
        }
    }

}
