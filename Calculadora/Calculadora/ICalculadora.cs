﻿
namespace Calculadora
{
    public interface ICalculadora
    {
        string Suma(string a, string b);
        string Resta(string a, string b);
        string Dividir(string a, string b);
        string Multiplicar(string a, string b);

    }
}
